angular.module("app").controller("TodoCtrl", function($scope, TodoSrv, $mdDialog) {
  $scope.titoloAngular = "La mia Todo List (Angular)";
  $scope.titoloMaterial = "La mia Todo List (Material)";

  $scope.lista = [];
  TodoSrv.getTodo().then(function(data) {
    $scope.lista = data;
    console.log($scope.lista);
    $scope.lista.forEach(function(el) {
      el.data = new Date(el.data);
    });
  });

  $scope.crea = function() {
    console.log($scope.nuovo);
    TodoSrv.createTodo($scope.nuovo).then(function(data) {
      $scope.nuovo = {};
      return TodoSrv.getTodo();
    }).then(function(data) {
      $scope.lista = data;
      $scope.lista.forEach(function(el) {
        el.data = new Date(el.data);
      });
    });
  }

  $scope.elimina = function(id) {
    console.log(id);
    TodoSrv.deleteTodo(id).then(function(data) {
      return TodoSrv.getTodo();
    }).then(function(data) {
      $scope.lista = data;
      $scope.lista.forEach(function(el) {
        el.data = new Date(el.data);
      });
    });
  }

  $scope.fatto = function(fatto, id) {
    console.log(fatto);
    console.log(id);
    TodoSrv.updateFatto(fatto, id).then(function(data) {
      return TodoSrv.getTodo();
    }).then(function(data) {
      $scope.lista = data;
      $scope.lista.forEach(function(el) {
        el.data = new Date(el.data);
      });
    });
  }
});
