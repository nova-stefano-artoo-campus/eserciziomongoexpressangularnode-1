angular.module("app").config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");
  $urlRouterProvider.when("", "/");

  $stateProvider.state("/", {
    url: "/",
    templateUrl: "Js/Todo/Todo.template.html",
    controller: "TodoCtrl"
  });
});
