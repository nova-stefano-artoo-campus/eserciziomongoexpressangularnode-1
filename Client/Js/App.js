angular.module("app", ["ui.router", "ngMaterial"]).run(function($rootScope) {
  console.log("App is started");
  $rootScope.currentNavItem = "/";
}).config(function($mdThemingProvider) {
  $mdThemingProvider.theme("default")
  .primaryPalette("indigo")
  .accentPalette("green")
  .warnPalette("red")
  .backgroundPalette("grey");
});
