var path = require("path");
var bodyParser = require("body-parser");

module.exports = function(app, express) {
  app.use(bodyParser.json());

  app.use("/bootstrap", express.static(path.join(__dirname, "..", "..", "node_modules", "bootstrap", "dist")));
  app.use("/jquery", express.static(path.join(__dirname, "..", "..", "node_modules", "jquery", "dist")));
  app.use("/angular", express.static(path.join(__dirname, "..", "..", "node_modules", "angular")));
  app.use("/angular-aria", express.static(path.join(__dirname, "..", "..", "node_modules", "angular-aria")));
  app.use("/angular-animate", express.static(path.join(__dirname, "..", "..", "node_modules", "angular-animate")));
  app.use("/angular-material", express.static(path.join(__dirname, "..", "..", "node_modules", "angular-material")));
  app.use("/angular-ui-router", express.static(path.join(__dirname, "..", "..", "node_modules", "angular-ui-router")));

  app.use("/js", express.static(path.join(__dirname, "..", "..", "Client", "Js")));
  app.use("/css", express.static(path.join(__dirname, "..", "..", "Client", "Css")));

  app.use("/", express.static(path.join(__dirname, "..", "..", "Client")));

  //rotta index
  app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname, "..", "..", "Client", "Index.html"));
  });

  //rotta per le todolist
  app.use("/api/todolist", require("./../Risorse"));
};
