var express = require("express");
var app = express();
const PORT = 3000;

//connessione al database
require("./Config/Database.js");

require("./Routes/Routes.js")(app, express);

//start del server
app.listen(PORT, function() {
  console.log("server in ascolto su http://localhost" + PORT);
});
