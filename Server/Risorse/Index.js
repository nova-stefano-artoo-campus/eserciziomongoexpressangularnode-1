var express = require("express");
var router = express.Router();
var todo = require("./Todo.controller.js")

router.post("/", todo.createTodo);

router.get("/", todo.getTodo);

router.delete("/:id([0-9a-z]{24})", todo.deleteTodo);

router.put("/:id([0-9a-z]{24})", todo.updateFatto);

module.exports = router;
