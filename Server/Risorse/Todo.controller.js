var mongoose = require("mongoose");
var Todo = require("./Todo.model.js");

module.exports = (function() {
  var createTodo = function(req, res) {
    var nuovo = new Todo(req.body);
    nuovo.save().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var getTodo = function(req, res) {
    Todo.find().exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var deleteTodo = function(req, res) {
    Todo.findByIdAndRemove(req.params.id).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var updateFatto = function(req, res) {
    Todo.findByIdAndUpdate(req.params.id, req.body).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  return {
    createTodo: createTodo,
    getTodo: getTodo,
    deleteTodo: deleteTodo,
    updateFatto: updateFatto
  }
})();
