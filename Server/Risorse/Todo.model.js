var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var todoSchema = new Schema({
  titolo: {
    type: String,
    required: [true, "Inserisci il titolo"]
  },
  descrizione: {
    type: String,
    required: [true, "Inserisci la descrizione"]
  }, fatto: {
    type: Boolean,
    default: false
  },
  data: {
    type: Date
  }
});

todoSchema.pre("save", function(next) {
  if(!this.data) {
    this.data = new Date();
  }
  next();
});

var Todo = mongoose.model("Todo", todoSchema);
module.exports = Todo;
